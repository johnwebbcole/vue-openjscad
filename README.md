# Vue OpenJscad

This is a Vue component wrapping an OpenJSCAD viewer. The component will respond to container resizes and generate the parameters table and export utilities.

This is intended as a way to embed a 3D viewer in a Vue application.

> Caution: You can have only one element of `open-jscad`!

Currently, you can only have one element on a page. There is an issue where OpenJSCAD only renders to the last element.

OpenJscad example:

```js
<open-jscad
    design="gearset.jscad"
    :panel="{ size: 223 }"
    :camera="{
    position: { x: 0, y: 0, z: 150 },
    clip: { min: 1, max: 500 }
    }"
></open-jscad>
```

## Installation

To add `vue-openjscad` to a project, follow these steps.

First, install the package.

```shell
npm install @jwc/vue-OpenJscad
```

Import the package into your page and add it to the components list.

```html static
<script>
  import OpenJscad from "@jwc/vue-openjscad";

  export default {
    name: "app",
    components: {
      OpenJscad
    }
  };
</script>
```

Add an `open-jscad` element to your template.

```html static
<open-jscad
  design="gearset.jscad"
  :panel="{ size: 223 }"
  :camera="{
    position: { x: 0, y: 0, z: 150 },
    clip: { min: 1, max: 500 }
    }"
></open-jscad>
```

`design` is a url where a `.jscad` file is located.
The component uses the `fetch` api to retrieve the desing
and load it into the OpenJsCAD processor.

## Project setup

```shell
npm install
```

### Compiles and hot-reloads for development

```shell
npm run serve
```

### Compiles and minifies for production

```shell
npm run build
```

### Run your tests

```shell
npm run test
```

### Lints and fixes files

```shell
npm run lint
```

### Run your unit tests

```shell
npm run test:unit
```
