## <small>1.0.3 (2019-01-05)</small>

* fix typo ([49c0e36](https://gitlab.com/johnwebbcole/vue-openjscad/commit/49c0e36))



## <small>1.0.2 (2019-01-05)</small>

* 1.0.2 ([d15ac0a](https://gitlab.com/johnwebbcole/vue-openjscad/commit/d15ac0a))
* add main and module ([284b6d7](https://gitlab.com/johnwebbcole/vue-openjscad/commit/284b6d7))



## <small>1.0.1 (2019-01-05)</small>

* 1.0.1 ([13c9bfd](https://gitlab.com/johnwebbcole/vue-openjscad/commit/13c9bfd))
* add cicd and styleguidist ([e305343](https://gitlab.com/johnwebbcole/vue-openjscad/commit/e305343))
* add gearset ([2c82b42](https://gitlab.com/johnwebbcole/vue-openjscad/commit/2c82b42))
* attempting to get pages working ([f3a093e](https://gitlab.com/johnwebbcole/vue-openjscad/commit/f3a093e))
* attempting to get pages working ([f508315](https://gitlab.com/johnwebbcole/vue-openjscad/commit/f508315))
* cleanup ([6df1d63](https://gitlab.com/johnwebbcole/vue-openjscad/commit/6df1d63))
* init ([f159bf5](https://gitlab.com/johnwebbcole/vue-openjscad/commit/f159bf5))
* initial commit ([269d233](https://gitlab.com/johnwebbcole/vue-openjscad/commit/269d233))
* update cicd ([bc9e81c](https://gitlab.com/johnwebbcole/vue-openjscad/commit/bc9e81c))
* update cicd ([1dbe004](https://gitlab.com/johnwebbcole/vue-openjscad/commit/1dbe004))



