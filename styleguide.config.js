module.exports = {
  // set your styleguidist configuration here
  title: "Vue OpenJscad",
  defaultExample: false,
  showSidebar: true,
  assetsDir: "public",
  usageMode: "expand",
  sections: [
    {
      name: "Introduction",
      content: "README.md"
    },
    {
      name: "Documentation",
      components: "src/components/OpenJscad.vue"
    }
  ]
  // webpackConfig: {
  //   // custom config goes here
  // }
};
