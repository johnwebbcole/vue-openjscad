<template>
  <div class="openjscad">
    <div ref="viewerContext" oncontextmenu="return false;">
      <div ref="viewerdiv" v-once></div>
    </div>
    <canvas class="viewerCanvas" ref="viewerCanvas"></canvas>
    <div class="viewer-footer">
      <a @click="resetCamera">Reset Camera</a>
      <div v-if="statusData" class="error-overlay">{{ statusData.message }}</div>
    </div>

    <table ref="parameterstable"></table>
    <div v-if="formats">
      <select v-model="format" @change="clearOutputFile">
        <option v-for="format in formats" :value="format" :key="format.name">
          {{
          format.displayName
          }}
        </option>
      </select>
      <button @click="generateFile">Generate {{ format && format.extension | uppercase }}</button>
      <a
        v-if="outputFile"
        :href="outputFile.data"
        :download="downloadFileName"
      >Download {{ downloadFileName }}</a>
    </div>
  </div>
</template>

<script>
const debug = require("debug")("OpenJSCad.vue");
/**
 * @typedef XYZCoord
 * @type {object}
 * @property {number} x
 * @property {number} y
 * @property {number} z
 */

/**
 * @typedef MinMax
 * @type {object}
 * @property {number} min
 * @property {number} max
 */

/**
 * Camera settings
 * @typedef CameraSettings
 * @type {object}
 * @property {number} fov - field of view.
 * @property {XYZCoord} angle - view angle about XYZ axis.
 * @property {XYZCoord} position - initial position at XYZ.
 * @property {MinMax} clip - rendering outside this range is clipped.
 */

/**
 * @typedef RgbaColor
 * @type {object}
 * @property {number} r - red (values 0.0 - 1.0).
 * @property {number} g - green (values 0.0 - 1.0).
 * @property {number} b - blue (values 0.0 - 1.0).
 * @property {number} a - alpha (values 0.0 - 1.0).
 */

/**
 * Grid settings
 * @typedef GridSettings
 * @type {object}
 * @property {number} i - number of units between grid lines.
 * @property {RgbaColor} color - color.
 */

/**
 * @typedef PlateSettings
 * @type {object}
 * @property {boolean} draw draw or not (default: true).
 * @property {number} size plate size (X and Y) (default: 200).
 * @property {GridSettings} m minor grid settings.
 * @property {GridSettings} M major grid settings.
 */

/**
 *
 * @typedef PosNegColor
 * @type {object}
 * @property {RgbaColor} neg color in negative direction.
 * @property {RgbaColor} pos color in positive direction.
 */

/**
 * @typedef AxisSettings
 * @type {object}
 * @property {boolean} draw - draw or not (default: false).
 * @property {PosNegColor} x
 * @property {PosNegColor} y
 * @property {PosNegColor} z
 */

/**
 * @typedef LightGLOptions
 * @type {object}
 * @property {object} canvas - Uses the HTML canvas given in 'options' or creates a new one if necessary.
 * @property {number} width - width applied to the canvas created when `options.canvas` is not set. (default: 800).
 * @property {number} height - height applied to the canvas created when `options.canvas` is not set. (default: 600).
 * @property {number} alpha - The alpha channel is disabled by default because it usually causes unintended transparencies in the canvas.
 *
 */

/**
 * @typedef ProcessorOptions
 * @type {object}
 * @property {Element} viewerContext - The main element that jscad uses.  If no canvas element is set, jscad creates one.
 * @property {Element} viewerdiv - Jscad moves the canvas tag here, or creates one and places it here.
 * @property {Element} parameterstable - A Table element reference.  The design parameters will be added here.
 * @property {Element} viewerCanvas - A Canvas element reference that will be used to draw the GL canvas.
 */

/**
 * @typedef JscadViewerOptions
 * @type {object}
 * @property {PlateSettings} plate - The grid show in the viewer.
 * @property {CameraSettings} camera
 * @property {AxisSettings} axis
 * @property {LightGLOptions} glOptions - Options sent to `GL.create()`.
 * @property {object} processor - options used by the viewer processor.
 */

/**
 * A Vue.js wrapper for the OpenJSCad UMD module build.
 *
 * ### Usage:
 * ```
 * <open-jscad
 *   design="gearset.jscad"
 *   :panel="{ size: 223 }"
 *   :camera="{
 *     position: { x: 0, y: 0, z: 150 },
 *     clip: { min: 1, max: 1000 }
 *   }"
 * ></open-jscad>
 * ```
 */
export default {
  name: "OpenJscad",

  props: {
    /**
     * Url of the design. This must be a valid OpenJSCAD file.
     */
    design: {
      type: String,
      required: true
    },
    /**
     * Initial camera settings.
     *
     * **CameraSettings** Object
     *
     *  * `fov` **number** field of view.
     *  * `angle` **{x: number, y: number, z: number}** view angle about XYZ axis.
     *  * `position` **{x: number, y: number, z: number}** initial position at XYZ.
     *  * `clip` **{min: number, max: number}** rendering outside this range is clipped.
     * @type {CameraSettings}
     */
    camera: {
      type: Object,
      required: false,
      default: undefined
    },
    /**
     * You can enable drawing axes and set the colors for each.
     *
     * **AxisSettings** Object
     *
     *  * `draw` **boolean** draw or not (default: false).
     *  * `x` **object** x axis color
     *    - `pos` **{r: number, g: number, b: number, a: number}** positive direction color. (values 0.0 - 1.0)
     *    - `neg` **{r: number, g: number, b: number, a: number}** negative direction color. (values 0.0 - 1.0)
     *  * `x` **object** y axis color
     *    - `pos` **{r: number, g: number, b: number, a: number}** positive direction color. (values 0.0 - 1.0)
     *    - `neg` **{r: number, g: number, b: number, a: number}** negative direction color. (values 0.0 - 1.0)
     *  * `x` **object** z axis color
     *    - `pos` **{r: number, g: number, b: number, a: number}** positive direction color. (values 0.0 - 1.0)
     *    - `neg` **{r: number, g: number, b: number, a: number}** negative direction color. (values 0.0 - 1.0)
     *
     * @type {AxisSettings}
     */
    axis: {
      type: Object,
      required: false,
      default: undefined
    },
    /**
     * The panel is the grid shown in the viewer.  You can disable the grid or customize it.
     *
     * **PlateSettings** Object
     *
     *  * `draw` **boolean** draw or not (default: false).
     *  * `size` **number** size of the grid
     *  * `m` **object** minor grid settings
     *    - `i` **number** number of units between grid lines.
     *    - `color` **{r: number, g: number, b: number, a: number}** minor grid color. (values 0.0 - 1.0)
     *  * `M` **object** major grid settings
     *    - `i` **number** number of units between grid lines.
     *    - `color` **{r: number, g: number, b: number, a: number}** major grid color. (values 0.0 - 1.0)
     *
     * @type {PlateSettings}
     */
    panel: {
      type: Object,
      required: false,
      default: undefined
    },
    /**
     * @ignore
     */
    delay: Number
  },

  data() {
    return {
      formats: undefined,
      format: undefined,
      outputFile: undefined,
      status: undefined,
      statusData: undefined
    };
  },

  computed: {
    /**
     * @description computes the curent clientWidth and Height
     * @returns {any}
     */
    size() {
      var devicePixelRatio = window.devicePixelRatio || 1;
      return {
        width: this.$el.clientWidth / devicePixelRatio,
        height: this.$el.clientHeight / devicePixelRatio
      };
    },

    /**
     * @description the download filename
     * @returns {any}
     */
    downloadFileName() {
      if (!this.format) return "no format";
      return this.design.replace(/\.jscad$/, `.${this.format.extension}`);
    }
  },

  filters: {
    /**
     * @description returns the value with all upercase
     * @param {any} value
     * @returns {any}
     */
    uppercase: function(value) {
      return value && value.toUpperCase();
    }
  },

  /**
   * @description mounted
   */
  async mounted() {
    /**
     * require in openjscad in mounted to enable SSR
     * @see https://vuepress.vuejs.org/guide/using-vue.html#browser-api-access-restrictions
     */
    const openjscad = require("@jscad/web");
    this._jscadViewer = openjscad(this.$refs.viewerContext, {
      processor: {
        ...this.$refs,
        setStatus: (status, data) => {
          // this.setStatus(status, data);
          debug("setStatus", { design: this.design, status, data });
          this.status = status;
          this.statusData = data;
        },
        instantUpdate: true,
        onUpdate: this.onUpdate,
        useAsync: false
      },
      viewer: {
        axis: this.axis,
        camera: this.camera,
        panel: this.panel,
        glOptions: {
          canvas: this.$refs.viewerCanvas
        }
      }
    });
    this.status = `Fetching ${this.design}`;
    var source = await this.fetchJscadFile();

    this.status = `Processing ${this.design}`;
    debug("mounted setJsCad", this.design.slice(0, 50));
    this._jscadViewer.setJsCad(source, this.design);
  },
  methods: {
    /**
     * Call back used to set stats from jscadViewer
     * @param {any} status - jscadViewer processor status.
     * @param {any} data - Additional data associated with current status.
     */
    setStatus(status, data) {
      debug("setStatus", { design: this.design, status, data });
      this.status = status;
      this.statusData = data;
    },

    /**
     * Hook called when anything changes in the jscadViewer.
     * Inside the OpenJSCad, this is updated when `enableItems`
     * is called.
     * @param {updateInfo} data - Updated data
     */
    onUpdate(data) {
      debug("onUpdate", data);
      /**
       * Add the name of the format to the format object.
       */
      this.formats = Object.entries(data.formats).reduce(
        (formats, [name, format]) => {
          formats[name] = { name, ...format };
          return formats;
        },
        {}
      );
      if (!this.format) this.format = this.formats.stla;

      this.outputFile = data.outputFile;
    },

    /**
     * Asynchronously create an output file with the current `format`.  `onUpdate` will
     * be set with the outputFile information.
     */
    generateFile() {
      debug("generateFile");
      if (this._jscadViewer) {
        this._jscadViewer.generateOutputFile(this.format, this.$refs.download);
      }
    },

    /**
     * Clear the output file info.  Used when a parameter or file format changes.
     */
    clearOutputFile() {
      if (this._jscadViewer) {
        this._jscadViewer.clearOutputFile();
      }
    },

    /**
     * Reset the camera to the home position.
     */
    resetCamera() {
      this._jscadViewer.resetCamera();
    },

    /**
     * Use the browsers `fetch` api to retrieve the design.
     * @returns {any}
     */
    async fetchJscadFile() {
      await this.wait(this.delay);
      return fetch(this.design).then(response => {
        debug("response", response);
        return response.text();
      });
    },

    async wait(ms) {
      return new Promise(resolve => {
        setTimeout(resolve, ms);
      });
    }
  }
};
</script>

<style lang="css">
.openjscad .viewer-footer {
  display: flex;
}
.openjscad .viewer-footer a {
  padding-right: 1em;
}
.viewerCanvas {
  width: 100%;
  height: 480px;
}
#viewerContext .viewer canvas {
  width: 320px;
  height: 240px;
}

#viewerContext div .shift-scene {
  display: none;
}
</style>
